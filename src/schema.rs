table! {
    annotations (id) {
        id -> Int4,
        motivation -> Varchar,
        target -> Varchar,
        selector -> Text,
        created -> Timestamp,
        modified -> Timestamp,
    }
}
