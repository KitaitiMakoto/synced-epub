extern crate synced_epub;
extern crate actix_web;
extern crate listenfd;
extern crate diesel;
extern crate futures;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate env_logger;

use listenfd::ListenFd;
use actix_web::{server, http, App, HttpRequest, HttpResponse, fs, Error, AsyncResponder, HttpMessage, Query};

use synced_epub::{establish_connection, create_annotation, models::*};
use self::diesel::prelude::*;

use futures::{Future, Stream};

struct AppState;

fn main() {
    ::std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();
    let mut listenfd = ListenFd::from_env();
    let mut server = server::new(|| {
        vec![
            App::with_state(AppState {})
                .prefix("/api").resource("/", |r| {
                    r.method(http::Method::GET).with(current_position);
                    r.method(http::Method::POST).f(save_annotation);
                })
                .boxed(),
            App::new().handler("/", fs::StaticFiles::new(".").unwrap()).boxed()
        ]
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)
    } else {
        server.bind("0.0.0.0:8080").unwrap()
    };

    server.run();
}

#[derive(Deserialize, Debug)]
struct AnnotationQuery {
    target: String,
    motivation: String
}

fn current_position(query: Query<AnnotationQuery>) -> HttpResponse {
    use synced_epub::schema::annotations::dsl::*;

    let connection = establish_connection();
    let results = annotations
        .filter(motivation.eq(&query.motivation))
        .filter(target.eq(&query.target))
        .order(modified.desc())
        .limit(1)
        .load::<Annotation>(&connection)
        .expect("Error loading annotation");
    if results.is_empty() {
        HttpResponse::NotFound()
            .finish()
    } else {
        HttpResponse::Ok()
            .content_type("application/json")
            .json(&results[0])
    }
}

fn save_annotation(req: &HttpRequest<AppState>) -> Box<Future<Item = HttpResponse, Error = Error>> {
    req.payload()
        .concat2()
        .from_err()
        .and_then(|body| {
            let body_vec = body.to_vec();
            let body_str = std::str::from_utf8(&body_vec).unwrap();
            let new_anno = serde_json::from_str::<NewAnnotation>(&body_str)?;

            let conn = establish_connection();
            let anno = create_annotation(&conn, new_anno);

            Ok(HttpResponse::Ok()
               .content_type("application/json")
               .json(anno))
        })
        .responder()
}
