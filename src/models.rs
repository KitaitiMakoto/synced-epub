use diesel::prelude::*;
use chrono::NaiveDateTime;

#[derive(Debug, Queryable, Serialize, Deserialize)]
pub struct Annotation {
    pub id: i32,
    pub motivation: String,
    pub target: String,
    pub selector: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime
}

use super::schema::annotations;

#[derive(Debug, Insertable, Serialize, Deserialize)]
#[table_name="annotations"]
pub struct NewAnnotation {
    pub motivation: String,
    pub target: String,
    pub selector: String
}
