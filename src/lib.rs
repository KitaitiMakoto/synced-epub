#[macro_use]
extern crate diesel;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate chrono;

pub mod schema;
pub mod models;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use std::env;
use chrono::NaiveDateTime;

pub fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

use self::models::{Annotation, NewAnnotation};

pub fn create_annotation<'a>(conn: &PgConnection, new_annotation: NewAnnotation) -> Annotation {
    use self::schema::annotations;

    diesel::insert_into(annotations::table)
        .values(&new_annotation)
        .get_result(conn)
        .expect("Error saving new annotation")
}
