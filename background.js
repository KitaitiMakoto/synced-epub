const appCacheName = 'synced-epub-app-2';
const appFiles = [
  '/',
  '/index.html',
  '/index.css',
  '/index.js',
  '/node_modules/sanitize.css/sanitize.css',
  '/node_modules/epubjs/dist/epub.min.js',
  '/node_modules/idb/lib/idb.js'
];

const dataCacheName = 'synced-epub-data';

self.addEventListener('install', event => {
  var appCached = caches.open(appCacheName)
      .then(cache => cache.addAll(appFiles));
  event.waitUntil(appCached);
});

self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys().then(keyList => {
      return Promise.all(keyList.map(key => {
        if ((key !== appCacheName) && (key !== dataCacheName)) {
          return caches.delete(key);
        }
      }))
    })
  );
});

self.addEventListener('fetch', event => {
  var requestUrl = new URL(event.request.url);
  if ((location.host === requestUrl.host) && (! appFiles.includes(requestUrl.pathname)) ) {
    var response = caches.match(event.request.clone(), {ignoreSearch: true})
        .then(res => {
          if (res) {
            caches.open(dataCacheName)
              .then(cache => cache.add(event.request));
            return res;
          } else {
            return fetch(event.request.url)
              .then(r => {
                if (! r.ok) {
                  throw new TypeError('bad request status');
                }
                caches.open(dataCacheName)
                  .then(cache => cache.put(event.request.url, r));
                return r.clone();
              });
          }
        });
  } else {
    var response = caches.match(event.request, {ignoreSearch: true})
        .then(res => res || fetch(event.request));
  }
  event.respondWith(response);
});
