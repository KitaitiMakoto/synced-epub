FROM rust
WORKDIR /usr/src/synced-epub
COPY . .
RUN cargo build --release
EXPOSE 8080
CMD ["./target/release/synced-epub"]
