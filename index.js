const INDEXEDDB_DATABASE_NAME = 'synced-epub';

// navigator.serviceWorker.register('background.js')
//   .catch(reason => console.error(reason));

setupBook()
  .then(book => {
    book.ready
      .then(([manifest, spine, metadata, cover, navigation, resources]) => {
        return loadCurrentPosition(book.packaging.uniqueIdentifier, book.packaging.metadata.modified_date)
          .then(currentPosition => displayBook(book, currentPosition))
          .catch(reason => {
            console.error(reason);
            return displayBook(book);
          })
          .then(() => setupReader(book));
      })
      .catch(reason => console.error(reason));
  });

function setupBook() {
  var url = new URL(document.location);
  var params = url.searchParams;
  var packageURI = params.get('book');
  const book = ePub(packageURI);

  return Promise.resolve(book);
}

function displayBook(book, currentPosition) {
  return book.renderTo('area').display(currentPosition)
    .then(() => {
      var tocs = document.querySelectorAll('.toc');
      tocs.forEach(toc => {
        var ol = toc.getElementsByTagName('ol')[0];
        book.spine.each(item => {
          var li = document.createElement('li');
          var button = document.createElement('button');
          button.dataset.href = item.href;
          var sectionIndex = book.navigation.tocByHref[item.href];
          if (sectionIndex === undefined) {
            button.textContent = item.href;
          } else {
            button.textContent = book.navigation.toc[sectionIndex].label;
          }
          button.addEventListener('click', event => {
            var href = event.target.dataset.href;
            book.rendition.display(href);
          });
          li.appendChild(button);
          ol.appendChild(li);
        });
      });
    });
}

function loadCurrentPosition(uniqueIdentifier, modified) {
  var remote = loadCurrentPositionFromRemoteStore(uniqueIdentifier, modified);
  var local = loadCurrentPositionFromIndexedDB(uniqueIdentifier, modified);

  return remote
    .then(remoteCFI => {
      return local.then(localCFI => {
        if (ePub.CFI.prototype.compare(remoteCFI, localCFI) < 0) {
          // TODO: save current position to remote store
          return localCFI;
        } else {
          // TODO: save current position to local store
          return remoteCFI;
        }
      })
        .catch(reason => {
          console.error(reason);
          return remoteCFI;
        });
    })
    .catch(reason => {
      console.error(reason);
      return local;
    });
}

function loadCurrentPositionFromRemoteStore(uniqueIdentifier, modified) {
    var params = new URLSearchParams();
    var target = `https://epub.directory/${encodeURIComponent(uniqueIdentifier)}/${modified}`;
    params.set('target', target);
    params.set('motivation', 'bookmarking');
    var url = `/api/?${params}`
    return fetch(url, {mode: 'cors'})
      .then(response => {
        if (response.ok) {
          return response.json()
            .then(data => {
              var selector = JSON.parse(data.selector);
              return selector.value;
            });
        }

        throw new Error("Loading current position failed.");
      })
}

function loadCurrentPositionFromIndexedDB(uniqueIdentifier, modified) {
  var storeName = `${uniqueIdentifier}@${modified}`;

  return idb.open(INDEXEDDB_DATABASE_NAME, 1, upgradeDB => {
    if (! upgradeDB.objectStoreNames.contains(storeName)) {
      upgradeDB.createObjectStore(storeName);
    }
  })
    .then(db => {
      var tx = db.transaction(storeName);
      var store = tx.objectStore(storeName);

      return store.get('currentPosition');
    });
}

function setupReader(book) {
  var rendition = book.rendition;

  var prevButtons = document.querySelectorAll('.controls .prev');
  var nextButtons = document.querySelectorAll('.controls .next');
  attachButtonHandler(prevButtons, 'prev');
  attachButtonHandler(nextButtons, 'next');

  var currentIndex = 0;
  var currentCFI = null;

  rendition.on('relocated', location => {
    prevButtons.forEach(button => {
      button.disabled = !!location.atStart;
    });
    nextButtons.forEach(button => {
      button.disabled = !!location.atEnd;
    });

    if (location.start.index > currentIndex) {
      currentIndex = location.start.index;
      currentCFI = location.start.cfi;
      var identifier = book.package.uniqueIdentifier;
      var modified = book.package.metadata.modified_date;
      var storeName = `${identifier}@${modified}`;
      saveCurrentPosition(storeName, identifier, modified, currentCFI)
        .catch(reason => console.error(reason));
    }
  });

  function saveCurrentPosition(storeName, uniqueIdentifier, modified, cfi) {
    var data = {
      motivation: 'bookmarking',
      target: `https://epub.directory/${uniqueIdentifier}/${modified}`,
      selector: JSON.stringify({ // FIXME
        type: "FragmentSelector",
        conformsTo: "http://www.idpf.org/epub/linking/cfi/epub-cfi.html",
        value: cfi
      })
    };

    return Promise.all([
      saveCurrentPositionToRemoteStore('/api/', data),
      saveCurrentPositionToIndexedDB(storeName, cfi)
    ]);
  }

  function saveCurrentPositionToIndexedDB(storeName, cfi) {
    return idb.open(INDEXEDDB_DATABASE_NAME, 1, upgradeDB => {
      if (! upgradeDB.objectStoreNames.contains(storeName)) {
        upgradeDB.createObjectStore(storeName);
      }
    })
      .then(db => {
        // TODO: create index
        var tx = db.transaction(storeName, 'readwrite');
        var store = tx.objectStore(storeName);
        store.put(cfi, 'currentPosition');

        return tx.complete;
    });
  }

  function saveCurrentPositionToRemoteStore(uri, data) {
    return fetch(uri, {
      method: 'POST',
      mode: 'cors',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify(data)
    });
  }

  function attachButtonHandler(buttons, direction) {
    buttons.forEach(button => {
      button.addEventListener('click', () => {
        rendition[direction]()
          .catch(reason => console.error(reason));
      });
    });
  }

  return Promise.resolve(rendition);
}
